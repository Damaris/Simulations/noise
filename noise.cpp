//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// Copyright (c) 2015-2018, Lawrence Livermore National Security, LLC.
// 
// Produced at the Lawrence Livermore National Laboratory
// 
// LLNL-CODE-716457
// 
// All rights reserved.
// 
// This file is part of Ascent. 
// 
// For details, see: http://ascent.readthedocs.io/.
// 
// Please also read ascent/LICENSE
// 
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice, 
//   this list of conditions and the disclaimer below.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the disclaimer (as noted below) in the
//   documentation and/or other materials provided with the distribution.
// 
// * Neither the name of the LLNS/LLNL nor the names of its contributors may
//   be used to endorse or promote products derived from this software without
//   specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL LAWRENCE LIVERMORE NATIONAL SECURITY,
// LLC, THE U.S. DEPARTMENT OF ENERGY OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
// DAMAGES  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
// IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
// POSSIBILITY OF SUCH DAMAGE.
// 
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//#define PARALLEL

#include "open_simplex_noise.h"

//#include <ascent.hpp>
#include <assert.h>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include "Damaris.h"

#include <unistd.h>



#ifdef PARALLEL  // ///////////////// ------------------------------------??????
#include </home/hadi/local/include/mpi.h>
#endif


struct Options
{
  int    m_dims[3];
  double m_spacing[3];
  int    m_time_steps;
  double m_time_delta;
  Options()
    : m_dims{32,32,32},
      m_time_steps(10),
      m_time_delta(0.5)
  {
    SetSpacing();
  }
  void SetSpacing()
  {
    m_spacing[0] = 10. / double(m_dims[0]);
    m_spacing[1] = 10. / double(m_dims[1]);
    m_spacing[2] = 10. / double(m_dims[2]);
  }
  void Parse(int argc, char** argv)
  {
    for(int i = 1; i < argc; ++i)
    {
      if(contains(argv[i], "--dims="))
      {
        std::string s_dims;
        s_dims = GetArg(argv[i]); 
        std::vector<std::string> dims;
        dims = split(s_dims, ',');

        if(dims.size() != 3)
        {
          Usage(argv[i]);
        }

		m_dims[0] = stoi(dims[0]);
        m_dims[1] = stoi(dims[1]);
        m_dims[2] = stoi(dims[2]);
        SetSpacing(); 
      }
      else if(contains(argv[i], "--time_steps="))
      {

        std::string time_steps;
        time_steps = GetArg(argv[i]); 
        m_time_steps = stoi(time_steps); 
      }
      else if(contains(argv[i], "--time_delta="))
      {

        std::string time_delta;
        time_delta= GetArg(argv[i]); 
        m_time_delta = stof(time_delta); 
      }
      else
      {
        Usage(argv[i]);
      }
    }
  }

  std::string GetArg(const char *arg)
  {
    std::vector<std::string> parse;
    std::string s_arg(arg);
    std::string res;

    parse = split(s_arg, '=');

	if (parse.size() != 2)
    {
      Usage(arg);
    }
    else
    {
      res = parse[1];
    } 
    return res;
  }
  void Print() const
  {
    std::cout<<"======== Noise Options =========\n";
    std::cout<<"dims       : ("<<m_dims[0]<<", "<<m_dims[1]<<", "<<m_dims[2]<<")\n"; 
    std::cout<<"spacing    : ("<<m_spacing[0]<<", "<<m_spacing[1]<<", "<<m_spacing[2]<<")\n"; 
    std::cout<<"time steps : "<<m_time_steps<<"\n"; 
    std::cout<<"time delta : "<<m_time_delta<<"\n"; 
    std::cout<<"================================\n";
  }

  void Usage(std::string bad_arg)
  {
    std::cerr<<"Invalid argument \""<<bad_arg<<"\"\n";
    std::cout<<"Noise usage: "
             <<"       --dims       : global data set dimensions (ex: --dims=32,32,32)\n"
             <<"       --time_steps : number of time steps  (ex: --time_steps=10)\n"
             <<"       --time_delta : amount of time to advance per time step  (ex: --time_delta=0.5)\n";
    exit(0);
  }

	std::vector<std::string> &split(const std::string &s, 
                                  char delim, 
                                  std::vector<std::string> &elems)
	{   
		std::stringstream ss(s);
		std::string item;

		while (std::getline(ss, item, delim))
		{   
			 elems.push_back(item);
		}
		return elems;
	 }
	 
	std::vector<std::string> split(const std::string &s, char delim)
	{   
		std::vector<std::string> elems;
		split(s, delim, elems);
		return elems;
	} 

	bool contains(const std::string haystack, std::string needle)
	{
		std::size_t found = haystack.find(needle);
		return (found != std::string::npos);
	}
};


struct SpatialDivision
{
  int m_mins[3];
  int m_maxs[3];

  SpatialDivision()
    : m_mins{0,0,0},
      m_maxs{1,1,1}
  {

  }

  bool CanSplit(int dim)
  {
	return m_maxs[dim] - m_mins[dim] + 1 > 1;
  }

  SpatialDivision Split(int dim)
  {
    SpatialDivision r_split;
    r_split = *this;
    assert(CanSplit(dim));
    int size = m_maxs[dim] - m_mins[dim] + 1;
    int left_offset = size / 2;   
  
    //shrink the left side
    m_maxs[dim] = m_mins[dim] + left_offset - 1;
    //shrink the right side
    r_split.m_mins[dim] = m_maxs[dim] + 1;
    return r_split;    
  }
};

struct DataSet
{
   const int  m_cell_dims[3];
   const int  m_point_dims[3];
   const int  m_cell_size;
   const int  m_point_size;
   double    *m_nodal_scalars;
   double    *m_zonal_scalars;
   double     m_spacing[3];
   double     m_origin[3];
   double     m_time_step;

   DataSet(const Options &options, const SpatialDivision &div)
     : m_cell_dims{div.m_maxs[0] - div.m_mins[0] + 1, 
                   div.m_maxs[1] - div.m_mins[1] + 1,
                   div.m_maxs[2] - div.m_mins[2] + 1},
       m_point_dims{m_cell_dims[0] + 1, 
                    m_cell_dims[1] + 1, 
                    m_cell_dims[2] + 1},
       m_cell_size(m_cell_dims[0] * m_cell_dims[1] * m_cell_dims[2]),
       m_point_size(m_point_dims[0] * m_point_dims[1] * m_point_dims[2]),
       m_spacing{options.m_spacing[0],
                 options.m_spacing[1],
                 options.m_spacing[2]},
       m_origin{0. + double(div.m_mins[0]) * m_spacing[0],
                0. + double(div.m_mins[1]) * m_spacing[1],
                0. + double(div.m_mins[2]) * m_spacing[2]}

   {
     m_nodal_scalars = new double[m_point_size]; 
     m_zonal_scalars = new double[m_cell_size]; 
   }    

   inline void GetCoord(const int &x, const int &y, const int &z, double *coord)
   {
      coord[0] = m_origin[0] + m_spacing[0] * double(x); 
      coord[1] = m_origin[1] + m_spacing[1] * double(y); 
      coord[2] = m_origin[2] + m_spacing[2] * double(z); 
   }  
   inline void SetPoint(const double &val, const int &x, const int &y, const int &z)
   {
     const int offset = z * m_point_dims[0] * m_point_dims[1] +
                        y * m_point_dims[0] + x;
     m_nodal_scalars[offset] = val;
   } 

   inline void SetCell(const double &val, const int &x, const int &y, const int &z)
   {
     const int offset = z * m_cell_dims[0] * m_cell_dims[1] +
                        y * m_cell_dims[0] + x;
     m_zonal_scalars[offset] = val;
   } 
    
   /*void PopulateNode(conduit::Node &node)
   {
      node["coordsets/coords/type"] = "uniform";

      node["coordsets/coords/dims/i"] = m_point_dims[0];
      node["coordsets/coords/dims/j"] = m_point_dims[1];
      node["coordsets/coords/dims/k"] = m_point_dims[2];

      node["coordsets/coords/origin/x"] = m_origin[0];
      node["coordsets/coords/origin/y"] = m_origin[1];
      node["coordsets/coords/origin/z"] = m_origin[2];

      node["coordsets/coords/spacing/dx"] = m_spacing[0];
      node["coordsets/coords/spacing/dy"] = m_spacing[1];
      node["coordsets/coords/spacing/dz"] = m_spacing[2];
    
      node["topologies/mesh/type"]     = "uniform";
      node["topologies/mesh/coordset"] = "coords";

      node["fields/nodal_noise/association"] = "vertex";
      node["fields/nodal_noise/type"]        = "scalar";
      node["fields/nodal_noise/topology"]    = "mesh";
      node["fields/nodal_noise/values"].set_external(m_nodal_scalars);

      node["fields/zonal_noise/association"] = "element";
      node["fields/zonal_noise/type"]        = "scalar";
      node["fields/zonal_noise/topology"]    = "mesh";
      node["fields/zonal_noise/values"].set_external(m_zonal_scalars);
   }*/

   void Print()
   {
     std::cout<<"Origin "<<"("<<m_origin[0]<<" -  "
                         <<m_origin[0] + m_spacing[0] * m_cell_dims[0]<<"), "
                         <<"("<<m_origin[1]<<" -  "
                         <<m_origin[1] + m_spacing[1] * m_cell_dims[1]<<"), "
                         <<"("<<m_origin[2]<<" -  "
                         <<m_origin[2] + m_spacing[2] * m_cell_dims[2]<<")\n ";
   }

   ~DataSet()
   {
     if(m_nodal_scalars) delete[] m_nodal_scalars; 
     if(m_zonal_scalars) delete[] m_zonal_scalars; 
   }
private:
  DataSet()
  : m_cell_dims{1,1,1},
    m_point_dims{2,2,2},
    m_cell_size(1),
    m_point_size(8)
  {
    m_nodal_scalars = NULL; 
    m_zonal_scalars = NULL; 
  };
};


void Init(SpatialDivision &div, const Options &options , MPI_Comm comm)
{
#ifdef PARALLEL

  //MPI_Init(NULL,NULL); // -> Moved to the main after damaris_initialize(...)
  int comm_size;
  MPI_Comm_size(comm, &comm_size);
  int rank;
  MPI_Comm_rank(comm, &rank);
  if(rank == 0) options.Print(); 
  std::vector<SpatialDivision> divs; 
  divs.push_back(div);
  int avail = comm_size - 1;
  int current_dim = 0;
  int missed_splits = 0;
  const int num_dims = 3;
  while(avail > 0)
  {
    const int current_size = divs.size();
    int temp_avail = avail;
    for(int i = 0; i < current_size; ++i)
    {
      if(avail == 0) break;
      if(!divs[i].CanSplit(current_dim))
      {
        continue;
      }
      divs.push_back(divs[i].Split(current_dim));
      --avail;
    }      
    if(temp_avail == avail)
    {
      // dims were too small to make any spit
      missed_splits++;
      if(missed_splits == 3)
      {
        // we tried all three dims and could
        // not make a split.
        for(int i = 0; i < avail; ++i)
        {
          SpatialDivision empty;
          empty.m_maxs[0] = 0;
          empty.m_maxs[1] = 0;
          empty.m_maxs[2] = 0;
          divs.push_back(empty);
        }
        if(rank == 0)
        {
          std::cerr<<"** Warning **: data set size is too small to"
                   <<" divide between "<<comm_size<<" ranks. "
                   <<" Adding "<<avail<<" empty data sets\n";
        }

        avail = 0; 
      }
    }
    else
    {
      missed_splits = 0;
    }

    current_dim = (current_dim + 1) % num_dims;
  }

  div = divs.at(rank);
#endif
  options.Print();
}

void Finalize()
{
#ifdef PARALLEL
  MPI_Finalize();
#endif
}

void InitParameters(const DataSet& dataset , const Options& options , int size)
{
	damaris_parameter_set("LOCAL_WIDTH" , &dataset.m_cell_dims[0] , sizeof(int));
	damaris_parameter_set("LOCAL_HEIGHT" , &dataset.m_cell_dims[1] , sizeof(int));
	damaris_parameter_set("LOCAL_DEPTH" , &dataset.m_cell_dims[2] , sizeof(int));

	damaris_parameter_set("WIDTH" , &options.m_dims[0] , sizeof(int));
	damaris_parameter_set("HEIGHT" , &options.m_dims[1] , sizeof(int));
	damaris_parameter_set("DEPTH" , &options.m_dims[2] , sizeof(int));

	damaris_parameter_set("size" , &size , sizeof(int));
}

void PrintParameters()
{
	int local_X , local_Y , local_Z;
	int X,Y,Z;
	int size;

	damaris_parameter_get("LOCAL_WIDTH" , &local_X , sizeof(int));
	damaris_parameter_get("LOCAL_HEIGHT" , &local_Y , sizeof(int));
	damaris_parameter_get("LOCAL_DEPTH" , &local_Z , sizeof(int));

	damaris_parameter_get("WIDTH" , &X , sizeof(int));
	damaris_parameter_get("HEIGHT" , &Y , sizeof(int));
	damaris_parameter_get("DEPTH" , &Z , sizeof(int));

	damaris_parameter_get("size" , &size , sizeof(int));

	std::cout << "local X,Y,Z are: " << local_X << "," << local_Y << "," << local_Z << std::endl;
	std::cout << "global X,Y,Z are: " << X << "," << Y << "," << Z << std::endl;
	std::cout << "size is: " << size << std::endl;
}

void WriteCoordinates(const SpatialDivision& div)
{
	int X = div.m_maxs[0] - div.m_mins[0] + 1;
	int Y = div.m_maxs[1] - div.m_mins[1] + 1;
	int Z = div.m_maxs[2] - div.m_mins[2] + 1;

	float* XCoord = new float[X+1];
	float* YCoord = new float[Y+1];
	float* ZCoord = new float[Z+1];

	for(int i=0; i<=X ; i++)
		XCoord[i] = div.m_mins[0] + i;

	for(int j=0; j<=Y ; j++)
		YCoord[j] = div.m_mins[1] + j;

	for(int k=0; k<=Z ; k++)
		ZCoord[k] = div.m_mins[2] + k;

	damaris_write("coord/x" , XCoord);
	damaris_write("coord/y" , YCoord);
	damaris_write("coord/z" , ZCoord);

	delete [] XCoord;
	delete [] YCoord;
	delete [] ZCoord;
}

int main(int argc, char** argv)
{

	MPI_Init(NULL,NULL);

	Options options;
	options.Parse(argc, argv);

	SpatialDivision div;
	//
	// Inclusive range. Ex cell dim = 32
	// then the div is [0,31]
	//
	div.m_maxs[0] = options.m_dims[0] - 1;
	div.m_maxs[1] = options.m_dims[1] - 1;
	div.m_maxs[2] = options.m_dims[2] - 1;

	damaris_initialize("noise.xml" , MPI_COMM_WORLD);

  int is_client;
  int err = damaris_start(&is_client);

  if ((err == DAMARIS_OK || err == DAMARIS_NO_SERVER) && is_client) {

	  MPI_Comm comm;
	  damaris_client_comm_get(&comm);

	  int rank,size;
	  MPI_Comm_rank(comm, &rank);
	  MPI_Comm_size(comm, &size);

	  Init(div, options , comm);
	  DataSet data_set(options, div);
	  InitParameters(data_set , options , size);


	  struct osn_context *ctx_zonal;
	  struct osn_context *ctx_nodal;
	  open_simplex_noise(77374, &ctx_nodal);
	  open_simplex_noise(59142, &ctx_zonal);

	  double time = 0;



	  WriteCoordinates(div);
	  //InitParameters(data_set , options , size);

	  //PrintParameters();

	  //if ((err == DAMARIS_OK || err == DAMARIS_NO_SERVER) && is_client) {

	  for(int t = 0; t < options.m_time_steps; ++t)
	  {
		  for(int z = 0; z < data_set.m_point_dims[2]; ++z)
			  for(int y = 0; y < data_set.m_point_dims[1]; ++y)
#ifdef NOISE_USE_OPENMP
#pragma omp parallel for
#endif
				  for(int x = 0; x < data_set.m_point_dims[0]; ++x)
				  {
					  double coord[4];
					  data_set.GetCoord(x,y,z,coord);
					  coord[3] = time;
					  double val_point = open_simplex_noise4(ctx_nodal, coord[0], coord[1], coord[2], coord[3]);
					  double val_cell = open_simplex_noise4(ctx_zonal, coord[0], coord[1], coord[2], coord[3]);
					  data_set.SetPoint(val_point,x,y,z);
					  if(x < data_set.m_cell_dims[0] &&
							  y < data_set.m_cell_dims[1] &&
							  z < data_set.m_cell_dims[2] )
					  {
						  data_set.SetCell(val_cell, x, y, z);
					  }
				  }

		  time += options.m_time_delta;


		  int64_t pos[3];

		  pos[0] = div.m_mins[0];
		  pos[1] = div.m_mins[1];
		  pos[2] = div.m_mins[2];

		  damaris_set_position("nodal_scalars" ,  pos);
		  damaris_write("nodal_scalars" , data_set.m_nodal_scalars);

		  damaris_set_position("zonal_scalars" ,  pos);
		  damaris_write("zonal_scalars" , data_set.m_zonal_scalars);

		  damaris_end_iteration();

		  sleep(3);

		  if (rank == 0) {
			  std::cout << "noise simulation: iteration " << t << " of " << options.m_time_steps << std::endl;
/*
			  std::cout << "Rank " << rank << " Point dims are: " << data_set.m_point_dims[0] <<  " , " <<
						   data_set.m_point_dims[1] <<  " , " <<
						   data_set.m_point_dims[2] <<  std::endl;

			  std::cout << "Rank " << rank  << " Cell dims are: " << data_set.m_cell_dims[0] <<  " , " <<
						   data_set.m_cell_dims[1] <<  " , " <<
						   data_set.m_cell_dims[2] <<  std::endl;

			  std::cout << "Rank " << rank  << " Mins are: " << div.m_mins[0] <<  " , " <<
						   div.m_mins[1] <<  " , " <<
						   div.m_mins[2] <<  std::endl;

			  std::cout << "Rank " << rank  << " Max are: " << div.m_maxs[0] <<  " , " <<
						   div.m_maxs[1] <<  " , " <<
						   div.m_maxs[2] <<  std::endl; */
		  }
	  } //for each time step

	  // cleanup
	  open_simplex_noise_free(ctx_nodal);
	  open_simplex_noise_free(ctx_zonal);
	  damaris_stop();
  }

  damaris_finalize();
  Finalize();
}
