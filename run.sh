#!/bin/sh
export LD_LIBRARY_PATH=$HOME/local/lib:$LD_LIBRARY_PATH 
export PATH=$HOME/local/bin:$PATH

#Run the example:
mpirun -np 3 ./noise --dims=128,128,128 --time_steps=100 --time_delta=0.5

